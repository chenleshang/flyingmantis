# README #

This repository is still not complete as grading requirement of not revealing code in public. As a compensation, the report is included temporarily. 

### What is this repository for? ###

* Final Project Collaboration for CIS 555. (Partial Private copy by Leshang)

### Deployment instructions ###

* To be continued. 

### Features Implemented ####

* Features implemented:
* Crawler: Starts with 250 seed websites, has a Master Node that keeps track of the state and status of the frontier queue and crawled urls. The Slave Nodes retrieves, parses, and stores relevant information. The documents for the Indexer is stored on S3 buckets, and the outgoing links are reformatted for the Page Ranker's ease of use. Auto timeout for bad hosts, while extracting the most information. Manual extracting/parsing on malformed documents. 
* Indexer: Filter special characters and strings in the body that are just sequences of numbers. Web pages that do not have valid bodies or cannot be parsed are not indexed and are therefore, not searchable.
* PageRank: Dealing with Dangling Links, Dealing with Sinks, Dealing with Self Loops, Encoding intermediate data, Convergence Test.
* Search Engine: Combines TFIDF and PageRank to output results related to search query, Web UI

### Extra Credit Claimed ###

* Indexer:
* Using Spark

* PageRank:
* Using Spark, Dealing with Dangling Links, Sinks, Self Loops, Encoding intermediate data.

* Search Engine: Augments search with results from calling Amazon's Product Advertisement API, some AJAX for dynamically displaying website

### Contribution guidelines ###

* 

### Who do I talk to? ###

* Bill He
* Mikael Mantis
* Leshang Chen <chenleshang@gmail.com>
* Steven Hwang