$(document).ready(function() {

      var ajaxRequest;

      navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = position;
            var ll = geolocation.coords.latitude + ',' + geolocation.coords.longitude;
            document.cookie = 'location=' + ll;
            console.log(document.cookie);
      });


      $('#searchBar').on('input', function(e) {
            var query = this.value;
            var url = '';
            var params='load=true&temp=' + query;
            if (window.XMLHttpRequest) {
                  ajaxRequest = new XMLHttpRequest();
            } else {
                  ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }

            ajaxRequest.open('POST', url, true);
            ajaxRequest.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                        $('.search').addClass('post');
                        $('.search').html(this.responseText);
                        $('#searchBar').focus().val($('#searchBar').val());
                  } else {
                        console.log(this.status);
                  }
            }

            ajaxRequest.send(params);
      })

      $('#searchButton').click(function(e) {
            e.preventDefault();
            var query = document.getElementById('searchBar').value;
            var url = '';
            var params = "query=" + query;
            if (window.XMLHttpRequest) {
                  //for modern web browsers
                  ajaxRequest = new XMLHttpRequest();
            } else {
                  //code for IE6, IE5 browsers
                  ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }

            ajaxRequest.open('POST', url, true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                        $('html').html(this.responseText);
                  } else {
                        console.log(this.status);
                  }
            }
            ajaxRequest.send(params);

      });


      function getCookie(cookieName) {
            var name = cookieName;
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                  var cookie = cookies[i];
                  while (cookie.charAt(0) == ' ') {
                        cookie = cookie.substring(1);
                  }
                  if (cookie.indexOf(cookieName) == 0) {
                        return cookie.substring(cookieName.length, cookie.length);
                  }
            }
            return "";
      }
});
