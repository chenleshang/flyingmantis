Full Name: Leshang Chen


Features implemented: PageRank: Dealing with Dangling Links, Dealing with Sinks, Dealing with Self Loops, Encoding intermediate data, Convergence Test. 
Extra Credit Claimed: Using Spark, Dealing with Dangling Links, Sinks, Self Loops, Encoding intermediate data. 
Instructions: For PageRank, just set boolean dev = true, pack it as JAR using maven and the associated pom.xml file, and upload to EMR to run. The data should be stored on S3 and output will be on RDS. You can run it on local machine by setting dev = false; 