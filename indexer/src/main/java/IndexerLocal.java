/* SimpleApp.java */
import org.apache.spark.api.java.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.SparkSession;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3Client;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;


public class IndexerLocal {

	
	private static final Pattern PUNCTUATION = Pattern.compile("[([.,=;!?:\\/\"\'<>]+)]");
	private static final Pattern SPACE = Pattern.compile("[\\s]");

	private static AWSCredentialsProvider s3Credentials;
	private static AmazonS3Client s3Client;

	private static HashMap<String,List<String>> inverted_index = new HashMap<String,List<String>>();

	private static Integer corpus_size = 0;

	/* Database connection information */
	DBInterface databaseInterface;
	static SparkSession spark;

	public static void main(String[] args) {

		boolean dev = false;


		spark = SparkSession
				.builder()
				.appName("Simple Application")
				.master("local[*]")
				.getOrCreate();

		final JavaSparkContext newjsc = new JavaSparkContext(spark.sparkContext());

		final String sourceDir;
		String corpusCountFile;
		corpusCountFile = "/home/cis555/workspace/spark-app/corpus/corpus.txt";
		sourceDir = "/home/cis555/workspace/spark-app/files/";
		
		
		

//		JavaPairRDD<String,String> webpagefiles = docIds.mapToPair(new PairFunction<String, String,String>(){
//
//			@Override
//			public Tuple2<String,String> call(String docId){
//				
//				String docfilepath = "/home/cis555/workspace/spark-app/files/" + docId;
//				JavaRDD<String> doc = spark.read().textFile(docfilepath);
//				List<String> lines = doc.collect();
//				
//				StringBuilder body = new StringBuilder();
//				for(String line : lines){
//					body.append(line);
//				}
//				//return newjsc.wholeTextFiles(docId);
//				return new Tuple2<String,String>(docId, body.toString());
//			}
//
//		});
		
		
//		File files = new File(sourceDir);
//		
//		File [] file_arr = files.listFiles();
//		List<String> filenames = new ArrayList<String>();
//		
//		for(File f : file_arr   ){
//			filenames.add(f.getName());
//		}
//		
//		JavaRDD<String> files_rdd = newjsc.parallelize(filenames);   
//		
//		JavaPairRDD<String,String> webpagefiles = files_rdd.mapToPair( new GetS3TextFilePair(newjsc) );
//		System.out.println(file_arr.toString());
		

//		JavaRDD<String> corpus_data = newjsc.textFile(corpusCountFile);
//		corpus_size = Integer.valueOf(corpus_data.first().trim()).intValue();
//
		JavaPairRDD<String,String> webpagefiles = newjsc.wholeTextFiles(sourceDir);

//		List<String> docs = new ArrayList<String>();
//		for(int i = 0; i < corpus_size + 1; i++){
//			docs.add( String.valueOf(i) );
//		}
//
//		JavaRDD<String> docIds = newjsc.parallelize(docs); 


		JavaRDD<Tuple2<String,String>> urlwordpairs = webpagefiles
				.flatMap(new FlatMapFunction<Tuple2<String, String>,Tuple2<String,String>>() {
					@Override
					public Iterator<Tuple2<String,String>> call(Tuple2<String, String> doctuple) {

						Document doc = Jsoup.parse(doctuple._2);

						String body_text = doc.body().text().trim();

						body_text = body_text.trim().replaceAll(PUNCTUATION.pattern(), " ");

						//System.out.println("body_text: " + body_text);

						String[] words = SPACE.split(body_text);
						ArrayList<Tuple2<String,String>> url_word_pair = new ArrayList<Tuple2<String,String>>();

						for( String word : words){

							Pattern pattern = Pattern.compile("^[a-zA-Z0-9]*$");
							Matcher matcher = pattern.matcher(word);
							if(matcher.matches()) {
								//System.out.println("first character int: " + (int)(word.charAt(0)) );

								if(word.trim().compareTo("") != 0 && word.length() > 1 ){
									String docid = doctuple._1().replace("file:"+sourceDir, "");
									url_word_pair.add(new Tuple2<String,String>(docid, word));
								}
							}
						}

						return url_word_pair.listIterator();
					}
				});


		JavaPairRDD<Tuple2<String,String>,Integer> ones = urlwordpairs.mapToPair(new PairFunction<Tuple2<String,String>,Tuple2<String,String>,Integer>(){
			@Override
			public Tuple2< Tuple2<String,String>, Integer> call(Tuple2<String,String> wordpairs){
				return 
						new Tuple2<Tuple2<String,String>, Integer> (
								new Tuple2<String,String>(wordpairs._1(),wordpairs._2()),  
								1
								);
			}
		});


		JavaPairRDD<Tuple2<String, String>, Integer> counts = ones.reduceByKey(
				new Function2<Integer, Integer, Integer>(){
					@Override
					public Integer call(Integer i1, Integer i2) {
						return i1 + i2;
					}
				}
				);


		// for augmented tf, get the count of word with the maximum count in a document

		JavaPairRDD<String,Integer> countsPerDoc = counts.mapToPair(new PairFunction<Tuple2<Tuple2<String,String>, Integer>, String, Integer> (){

			@Override
			public Tuple2<String, Integer> call(Tuple2<Tuple2<String, String>, Integer> tuple) throws Exception {

				return new Tuple2<String, Integer> ( tuple._1()._1(), tuple._2() ) ;
			}

		});

		JavaPairRDD <String,Integer> maxCountsPerDoc = countsPerDoc.reduceByKey(new Function2<Integer,Integer,Integer> (){

			@Override
			public Integer call(Integer v1, Integer v2) throws Exception {
				// TODO Auto-generated method stub
				return new Integer (Math.max(v1.intValue(), v2.intValue()));
			}

		});

		// now generate rdds to compute the tf scores.
		JavaPairRDD<String, Tuple2<String,Integer>> countsPerDoc_docpair = counts.mapToPair(
				new PairFunction< Tuple2< Tuple2<String,String>, Integer>, 
				String, Tuple2<String, Integer>> (){ // result pair (String, Tuple2)

					@Override
					public Tuple2<String, Tuple2<String, Integer>> call(Tuple2<Tuple2<String, String>, Integer> tuple) throws Exception {

						return new Tuple2<String, Tuple2<String, Integer>> ( tuple._1()._1(), new Tuple2<String,Integer>( tuple._1()._2(), tuple._2()) ) ;
					}

				});



		JavaRDD<Tuple4<String,String,Integer, Double>> countsPerDoc_with_tf = countsPerDoc_docpair.join(maxCountsPerDoc).map(
				new Function<Tuple2<String, Tuple2<Tuple2<String,Integer>,Integer>>, Tuple4<String,String,Integer,Double>> (){

					@Override
					public Tuple4<String, String, Integer, Double> call(
							Tuple2<String, Tuple2<Tuple2<String, Integer>, Integer>> tuple_pair) throws Exception {
						// TODO Auto-generated method stub
						String doc_url = tuple_pair._1();
						String word = tuple_pair._2()._1()._1();
						Integer count = tuple_pair._2()._1()._2();
						Double max_word_count = new Double(tuple_pair._2()._2());


						return new Tuple4<String, String, Integer, Double>(doc_url, word, count, new Double(  count.intValue() / max_word_count.doubleValue() ) );
					}

				});

		JavaRDD<Tuple3<String,String, Double>> doc_word_tf = countsPerDoc_with_tf.map(
				new Function<Tuple4<String,String,Integer,Double>, Tuple3<String,String,Double>> (){

					@Override
					public Tuple3<String, String, Double> call(Tuple4<String,String,Integer,Double> tuple) throws Exception {
						String doc_url = tuple._1();
						String word = tuple._2();
						Double tf = tuple._4();

						return new Tuple3<String, String, Double>(doc_url, word, tf  );
					}

				});
		
		doc_word_tf.collect();
		
		DBInterface.startDBConnection();
		doc_word_tf.foreach(new VoidFunction<Tuple3<String,String,Double>>(){
		
			@Override
			public void call(Tuple3<String, String, Double> t) throws Exception {
				// TODO Auto-generated method stub
				String word = t._2();
				int id = Integer.valueOf(t._1()).intValue();
				Double tfscore = t._3();
				DBInterface.setTFIDF(word, id, tfscore);
			}
		});
		DBInterface.closeDBConnection();

		System.out.println("DONE!");
		//spark.stop();
		newjsc.stop();
		//sc.stop();
	
	}
}








