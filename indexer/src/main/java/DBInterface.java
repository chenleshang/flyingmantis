import java.io.IOException;
import java.sql.*;
import java.util.Properties;

import oracle.jdbc.pool.OracleDataSource;

public class DBInterface {



	//static String hostName = "mydbinstance.ciynpeqwoevt.us-east-1.rds.amazonaws.com:1521";
	static String hostName =  "newdbinstance.cdc3aijmhcd9.us-east-1.rds.amazonaws.com:1521";
	//static String user = "chenleshang";
	//static String password = "chenleshang";
	static String user = "flyingmantis";
	static String password = "flyingmantis";


	static String database = "ORCL";

	
	Connection conn;
	Statement mystat = null;
	ResultSet myrs = null;


	static Integer batch_count = 0;
	static OracleDataSource oracleDS = null;
	static Object lock = new Object();


	public static void startDBDataSource(){

		Properties props = new Properties();

		oracleDS = null;
		try {

			oracleDS = new OracleDataSource();
			oracleDS.setURL(hostName);
			oracleDS.setUser(user);
			oracleDS.setPassword(password);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public  void commitStatement(){	

		synchronized(batch_count){


			if(batch_count > 0){
				synchronized(lock){
					try {
						mystat.executeBatch();
						mystat.clearBatch();
						batch_count = 0;
						mystat.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}


		}

	}

	public Connection startDBConnection(){	

		try {
			conn = DriverManager.getConnection("jdbc:oracle:thin:@//" + hostName
					+ "/" + database, user, password);

			return conn;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public void closeDBConnection(){

		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setTF(Connection conn,  String word, int id, Double score)
	{

		try
		{
			mystat = conn.createStatement();

			String insert = "INSERT INTO TF(WORD, DOCID, TFSCORE) VALUES ('" + word + "', " + id + "," + score + ")";

			ResultSet myrs = mystat.executeQuery(insert);
			myrs.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void setTFBatch(Connection conn,  String word, int id, Double score)
	{

		try
		{

			synchronized(batch_count){

				String insert = "INSERT INTO TF(WORD, DOCID, TFSCORE) VALUES ('" + word + "', " + id + "," + score + ")";

				batch_count ++;

				if(batch_count == 5000){

					int [] results;
					System.out.println("Executing Batch update!");
					synchronized(lock){
						results = mystat.executeBatch();
						mystat.clearBatch();
						mystat.close();
						mystat = conn.createStatement();
						batch_count = 0;
					}
					
				} else{

					synchronized(lock){
						if(mystat == null){

							mystat = conn.createStatement();
						}
						else {
							//System.out.println("mystat is closed" + mystat.isClosed());
							if(mystat.isClosed()){
								System.out.println("creating new mystat ");
								mystat = conn.createStatement();
							}else {
								System.out.println("Batching update....");
								mystat.addBatch(insert);
							}

						}

					}


				}

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}	


	public void computeTFIDFScores()
	{

		try
		{

			mystat = conn.createStatement();

			String corpus_count_query = " select count(distinct docid) as count from TF ";

			myrs = mystat.executeQuery(corpus_count_query);
			Integer corpus_size = 0;
			while(myrs.next() == true){

				corpus_size = Integer.valueOf(myrs.getString("count"));

			}

			myrs.close();

			mystat = conn.createStatement();

			String computeTFIDF = "insert into TFIDF (word, docid, TFIDFSCORE) select TF.word, TF.docid, ( TF.tfscore * LN( "+ corpus_size  +" / mycounts.total )) as tfidf "
					+ "from TF, (select word, count(docid) as total from TF group by TF.word ) mycounts "
					+ "where TF.word = mycounts.word";

			System.out.println(computeTFIDF);
			myrs =  mystat.executeQuery(computeTFIDF);   

			myrs.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void computeTFIDFScoresWithDS(int corpus_size)
	{

		try
		{
			Connection conn = oracleDS.getConnection();
			mystat = conn.createStatement();

			String computeTFIDF = "insert into TFIDF (word, docid, TFIDFSCORE) select TF.word, TF.docid, ( TF.tfscore * LN( "+ corpus_size  +" / mycounts.total )) as tfidf "
					+ "from TF, (select word, count(docid) as total from TF group by TF.word ) mycounts "
					+ "where TF.word = mycounts.word";
			System.out.println(computeTFIDF);
			mystat.executeUpdate(computeTFIDF);

			mystat.close();
			conn.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public static void main(String[] args){

		//args[0] is corpus size
		Integer corpus_size = Integer.valueOf(args[0]);
		//computeTFIDFScores(corpus_size);

		//    	deleteALLDocID();
		//    	
		//    	setDOCID(1, "www.google.com");
		//    	setDOCID(2, "www.yahoo.com");
		//    	
		//    	deleteALLTFIDF();
		//    	
		//    	setTFIDF("google", 1, 0.66);
		//    	setTFIDF("google", 2, 0.5);
		//    	setTFIDF("is", 1, 0.21);
		//    	setTFIDF("fun", 1, 0.9);
		//    	setTFIDF("yahoo", 1, 0.6);
		//    	setTFIDF("yahoo", 2, 0.13);
		//    	setTFIDF("website", 2, 0.4);
		//    	
		//    	
		//    	getAllDOCID();
		//    	getAllTFIDF();
		//    	
		//    	joinDOCIDPAGERANK();
		//    	System.out.println("DONE");

		//    	setTFIDF("yahoo", 2, 1.0);
		//    	
		//    	setDOCID(2, "www.yahoo.com");

	}
}
