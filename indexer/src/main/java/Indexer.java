/* SimpleApp.java */
import org.apache.spark.api.java.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.SparkSession;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.rdd.RDD;


public class Indexer {

	
	public static final class UploadToDB implements VoidFunction<Tuple3<String,String,Double>>{
		
		static Connection dbconn;
		static DBInterface mydbinterface;
		
		public UploadToDB(){
			
		    System.out.println("Creating a DB connection!!!!!");
		    mydbinterface = new DBInterface();
			dbconn = mydbinterface.startDBConnection();
			
		}
		
		public void commitLastBatch(){
			System.out.println("Commiting last batch");
			mydbinterface.commitStatement();
		}
		
		public void closeDBConnection() throws SQLException{
			System.out.println("CLOSing the DB connection!!!");
			dbconn.close();
		}
		
		@Override
		public void call(Tuple3<String, String, Double> t) throws Exception {
			// TODO Auto-generated method stub
			if( t != null){
				String stemmed_word = t._1();
				int beginning_slash = t._2().lastIndexOf("/");
				String filepath = t._2().substring(beginning_slash+1);
				int id = Integer.valueOf(filepath).intValue();
				Double tfscore = t._3();
				//System.out.println("uploading to the database " + stemmed_word +", "+id+", " + tfscore);
				//DBInterface.setTF(dbconn, stemmed_word, id, tfscore);
				mydbinterface.setTFBatch(dbconn, stemmed_word, id, tfscore);
			} else{
				System.out.println("got a null Tuple! ");
			}
		}
	}


	//private static final Pattern PUNCTUATION = Pattern.compile("[([.,=;!?:\\/\"\'<>]+)]");
	private static final Pattern SPACE = Pattern.compile("[\\s]");

	private static AWSCredentialsProvider s3Credentials = new InstanceProfileCredentialsProvider(true);
	private static AmazonS3Client s3Client = new AmazonS3Client(s3Credentials);
	//private static AWSCredentials credentials = new ProfileCredentialsProvider("default").getCredentials();
	//private static AmazonS3Client s3Client = new AmazonS3Client(credentials);
	private static FilterHelper filterhelper = new FilterHelper();
	static String sourceDir;
	

	public static void main(String[] args) throws SQLException {

		// set up the region for the S3Client
		Region usEast1 = Region.getRegion(Regions.US_EAST_1);
		s3Client.setRegion(usEast1);

		SparkSession spark = SparkSession
				.builder()
				.appName("Simple Application")
				.master("local[*]")
				.getOrCreate();

		JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

		String BUCKET_NAME = args[1];
		sourceDir = args[0];



//		jsc.hadoopConfiguration().set("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem"); 
//		jsc.hadoopConfiguration().set("fs.s3.awsAccessKeyId", s3Credentials.getCredentials().getAWSAccessKeyId() );
//		jsc.hadoopConfiguration().set("fs.s3.awsSecretAccessKey", s3Credentials.getCredentials().getAWSSecretKey() );


		JavaPairRDD<String,String> webpagefiles = jsc.wholeTextFiles(sourceDir, 40);
		
		
		
		JavaRDD<Tuple3<String,String,Double>> urlwordpairswithtf = webpagefiles 
				.flatMap(new FlatMapFunction<Tuple2<String, String>,Tuple3<String,String,Double>>() {

					@Override
					public Iterator<Tuple3<String,String, Double>> call(Tuple2<String, String> doctuple) {

						//local hash map for document words.
						HashMap<String,Integer> doc_index = new HashMap<String,Integer>();

						String docId = doctuple._1();
						Document doc = Jsoup.parse(doctuple._2);

						if( doc != null && doc.body() != null && doc.body().text() != null  ){

							System.out.println("parsing the body of doc " + docId);
							String body_text = doc.body().text().trim();

							String[] words = SPACE.split(body_text);
							ArrayList<Tuple3<String,String,Double>> url_word_pair = new ArrayList<Tuple3<String,String,Double>>();

							Integer max_word_count = 0;

							for( String word : words){

								Pattern pattern = Pattern.compile("^[a-zA-Z0-9]*$");

								Matcher matcher = pattern.matcher(word);
								if(matcher.matches()) {

									Pattern justanumberpattern = Pattern.compile("^[0-9]*$");
									Matcher numbermatcher = justanumberpattern.matcher(word);

									if(!numbermatcher.matches()){
										// get rid of single letters
										if(word.trim().compareTo("") != 0 && word.length() > 1 ){	

											// an attempt to filter gibberish character strings
											if(word.length() > 20 ){
												continue;
											}

											// all lower case
											word = word.toLowerCase();

											//filter stop words
											if( filterhelper.isStopWord(word) == true){
												continue;
											}

											// stem the word

											String stemmed_word = Stemmer2.stemString(word);

											if(doc_index.containsKey(stemmed_word) == true){
												Integer count = doc_index.get(stemmed_word);
												count++;
												if(count > max_word_count){ max_word_count = count; }

												doc_index.put(stemmed_word, count);

											}else{
												if(1 > max_word_count){ max_word_count = 1; }
												doc_index.put(stemmed_word,1);
											}

										}
									}
								}
							}

							// computed the idf scores.
							for( String stemmed_word : doc_index.keySet() ){
								url_word_pair.add(new Tuple3<String,String,Double>(stemmed_word,docId, Double.valueOf(doc_index.get(stemmed_word))/max_word_count      ));
							}

							doc_index = null;

							return url_word_pair.listIterator();
						} 
						else
						{ //no body to parse 
							System.out.println("Jsoup couldn't parse document with Id: " + docId);
							// just create a empty set to skip over this?
							ArrayList<Tuple3<String,String,Double>> url_word_pair = new ArrayList<Tuple3<String,String,Double>>();
							return url_word_pair.listIterator();
							
						}
					}
				}).repartition(3);
			

		List<Tuple3<String,String,Double>> tuples = urlwordpairswithtf.collect();
		
		File results = new File("results.txt");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter( results ));
			
			for( Tuple3<String,String,Double> t : tuples){
				
				String stemmed_word = t._1();
				int beginning_slash = t._2().lastIndexOf("/");
				String filepath = t._2().substring(beginning_slash+1);
				int id = Integer.valueOf(filepath).intValue();
				Double tfscore = t._3();
				
				String line = stemmed_word + "," +id +","+tfscore+"\n";
				try {
					writer.write(line);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			writer.close();
			// write this file to S3
			s3Client.putObject( BUCKET_NAME , "results2", results);
			
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		

		//Write to Database
		
//		VoidFunction function = new UploadToDB();
//		urlwordpairswithtf.foreach(function  );
//		
//		try{
//			((UploadToDB) function).commitLastBatch();
//			((UploadToDB) function).closeDBConnection();
//		} catch (SQLException e){
//			e.printStackTrace();
//			//System.exit(1);
//		}
//		urlwordpairswithtf.foreach(new VoidFunction<Tuple3<String,String,Double>>(){
//
//			@Override
//			public void call(Tuple3<String, String, Double> t) throws Exception {
//				// TODO Auto-generated method stub
//				if( t != null){
//					String stemmed_word = t._1();
//					//String filepath = t._2().replaceAll(sourceDir, "");
//					int beginning_slash = t._2().lastIndexOf("/");
//					String filepath = t._2().substring(beginning_slash+1);
//					int id = Integer.valueOf(filepath).intValue();
//					Double tfscore = t._3();
//					DBInterface.setTF(dbconn, stemmed_word, id, tfscore);
//				} else{
//					System.out.println("got a null Tuple! ");
//				}
//			}
//		});
		
		System.out.println("done with an Iteration");
		// unpersist it rdds
		urlwordpairswithtf.unpersist();
		webpagefiles.unpersist();
	
		System.out.println("DONE!");

		jsc.close();
		System.out.println("STOPPED!");
		System.exit(0);
	}
}








