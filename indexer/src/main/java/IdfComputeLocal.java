import java.util.Properties;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class IdfComputeLocal {

	private static Integer corpus_size = 0;
	static SparkSession spark;
	static SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("Simple Application");
	static SparkContext sc = new SparkContext(conf);
    private static final SparkSession sparkSession = new SparkSession(sc);
    final static String connection_url = "jdbc:oracle:thin:@//mydbinstance.ciynpeqwoevt.us-east-1.rds.amazonaws.com:1521/ORCL";
    
    public static void main(String[] args) {
		
	
		String corpusCountFile;
		corpusCountFile = "/home/cis555/workspace/spark-app/corpus/corpus.txt";
		
		RDD<String> corpus_data =  sc.textFile(corpusCountFile,1);
		corpus_size = Integer.valueOf(corpus_data.first().trim()).intValue();
		
		final Properties connectionProperties = new Properties();
		connectionProperties.put("user", "chenleshang");
		connectionProperties.put("password", "chenleshang");
		connectionProperties.put("driver","oracle.jdbc.driver.OracleDriver");
		
		final String sql_expr =
				  "(select TF.word, TF.docid, ( TF.tfscore * LN( "+ corpus_size  +" / mycounts.total )) as tfidf "
				+ "from TF, (select word, count(docid) as total from TF group by TF.word ) mycounts "
				+ "where TF.word = mycounts.word)";
		

		Dataset<Row> wordlist_lengths = sparkSession.read().jdbc(connection_url, sql_expr, connectionProperties);

		
		
		DBInterface.startDBConnection();
		wordlist_lengths.foreach(new ForeachFunction<Row>(){

			@Override
			public void call(Row t) throws Exception {
				
				//0 - word
				String word = t.getString(0);
				//1 - docid
				Integer docid = ((java.math.BigDecimal)t.get(1)).intValue() ;
				//2 - tfscore
				Double score = ((java.math.BigDecimal)t.get(2)).doubleValue();
				//System.out.println("updating db: (" + word+", "+docid+", "+score+")");
				DBInterface.setTFIDF(word , docid, score );
			}
			
		});
		DBInterface.closeDBConnection();
		
	}

}
