
public class ComputeIDF {

	
	public static void main(String[]args){
		
		DBInterface db = new DBInterface();
		db.startDBConnection();
    	db.computeTFIDFScores();
    	db.closeDBConnection();
		
	}
	
	
}
